#!/bin/bash

## run (only once) processes which spawn with the same name
function run {
   if (command -v $1 && ! pgrep $1); then
     $@&
   fi
}

log="/tmp/autorun.log"

date > $log

if (command -v  xfce4-power-manager && ! pgrep xfce4-power-man) ; then
    xfce4-power-manager &
fi

#run blueberry-tray
run pasystray
run nm-applet
run pcmanfm --daemon-mode
run gammy


echo "Success!!!" >> $log